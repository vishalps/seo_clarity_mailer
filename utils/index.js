const Winston = require('../logs/logger/winston');
const axios = require('axios');
const async = require('async');
module.exports = {
  pingObservingNodes(configs, maincb) {
    let downIps = [];
    async.eachSeries(configs, (item, callback) => {
      axios.get(item.ip + '/_stats').then((response) => {
        if (response.status === 200) {
          callback();
        } else {
          downIps.push(item.name);
          callback();
        }
      }).catch(e => {
        downIps.push(item.name);
        callback();
      });
    }, (error) => {
      if (error) {
        maincb(error);
      } else {
        if (!downIps.length)
          Winston.InfoLogging(`All Nodes Up`, `utils/index.js`, `55`);
        maincb(null, downIps)
      }
    });
  }
}