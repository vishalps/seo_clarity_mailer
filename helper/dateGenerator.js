const moment = require('moment');

module.exports = {
    createDate: function (param) {
        let date;
        switch (param) {
            case 'now':
                date = moment().format();
                break;
            case '1h':
                date = moment().subtract(1, 'hour').format();
                interval = 'hour'
                break;
            case '1y':
                date = moment().subtract(1, 'year').format();
                interval = 'day'
                break;
            case '24h':
                date = moment().subtract(1, 'days').format();
                interval = 'hour'
                break;
            case '1w':
                date = moment().subtract(1, 'weeks').format();
                interval = 'day'
                break;
            case '3M':
                date = moment().subtract(1, 'months').format();
                interval = 'day'
                break;
            case '1M':
                date = moment().subtract(1, 'months').format();
                interval = 'day'
                break;
        }
        return date;
    }

};