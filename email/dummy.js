const _ = require('lodash');
const transporter = require('./index');
const template = require('./templates');

function subjectMapper(params) {
    switch (params) {
        case 'index_rejection_alert':
            return {
                content: 'Index Rejection Alert'
            };
        case 'search_rejection_alert':
            return {
                content: 'Search Rejection Alert'
            };
        case 'memory_pressure_alert':
            return {
                content: 'Memory Pressure Alert'
            };
        case 'disk_usage_alert':
            return {
                content: 'Disk Usage Alert'
            };
        case 'single_node_overload_alert':
            return {
                content: 'Single Node Overload Alert'
            };
        case 'split_brain_alert':
            return {
                content: 'Split Brain Alert'
            };
        case 'node_out_alert':
            return {
                content: 'Node Down Alert'
            };
        case 'high_cpu_alert':
            return {
                content: 'High Cpu Alert'
            };
        case 'high_memory_alert':
            return {
                content: 'High Memory Alert'
            };
        case 'too_much_indexing_on_single_node':
            return {
                content: 'Too much indexing on a single node'
            };
        case 'shard_document_limit_approaching_alert':
            return {
                content: 'Shard Document Approaching Limit'
            };
        case 'shard_not_allocated_alert':
            return {
                content: 'Shard Not Allocated Alert'
            };
        case 'gc_taking_too_much_time_alert':
            return {
                content: 'Garbage Taking Much Time'
            };
        case 'too_much_documents_in_a_shard':
            return {
                content: 'Too Much Document In A Shard'
            };
        default:
            return {
                content: 'Pending Alert'
            };
    }
}
module.exports = {
    sendMail: (alert, params, callback) => {
        console.log('EMAILS TO BE SENT', JSON.stringify(params, null, 4));
        if (params.length > 0) {
            const mailOptions = {
                to: _.map(params, item => item.key),
                from: 'projectmanagement@factweavers.com',
                subject: subjectMapper(alert.type).content,
                text: 'Alert mail',
                html: template.alert(subjectMapper(alert.type), alert)
            };
            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    console.log('MAIL ERR');
                    console.log(error);
                    callback(error);
                } else {
                    console.log('Message sent: %s', info.messageId);
                    console.log('MAIL SUCCESS');
                    callback(null, params);
                }
            });
        } else {
            callback(null, params);
        }
    },
    subjectMapper(params) {
        switch (params) {
            case 'index_rejection_alert':
                return {
                    content: 'Index Rejection Alert'
                };
            case 'search_rejection_alert':
                return {
                    content: 'Search Rejection Alert'
                };
            case 'memory_pressure_alert':
                return {
                    content: 'Memory Pressure Alert'
                };
            case 'disk_usage_alert':
                return {
                    content: 'Disk Usage Alert'
                };
            case 'single_node_overload_alert':
                return {
                    content: 'Single Node Overload Alert'
                };
            case 'split_brain_alert':
                return {
                    content: 'Split Brain Alert'
                };
            case 'node_out_alert':
                return {
                    content: 'Node Down Alert'
                };
            case 'high_cpu_alert':
                return {
                    content: 'High Cpu Alert'
                };
            case 'high_memory_alert':
                return {
                    content: 'High Memory Alert'
                };
            case 'too_much_indexing_on_single_node':
                return {
                    content: 'Too much indexing on a single node'
                };
            case 'shard_document_limit_approaching_alert':
                return {
                    content: 'Shard Document Approaching Limit'
                };
            case 'shard_not_allocated_alert':
                return {
                    content: 'Shard Not Allocated Alert'
                };
            case 'gc_taking_too_much_time_alert':
                return {
                    content: 'Garbage Taking Much Time'
                };
            case 'too_much_documents_in_a_shard':
                return {
                    content: 'Too Much Document In A Shard'
                };
            default:
                return {
                    content: 'Pending Alert'
                };
        }
    }
};

