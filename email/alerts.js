const _ = require('lodash');
const sgMail = require('./index');
const template = require('./templates');
const Winston = require('../logs/logger/winston');

function subjectMapper(params) {
    switch (params) {
        case 'node_down_alert':
            return {
                content: 'Node Down Alert'
            };
        default:
            return {
                content: 'Pending Alert'
            };
    }
}
module.exports = {
    sendMail: (alert, params, callback) => {
        if (params.length > 0) {
            var body = null;
            if (alert.type === 'node_down_alert') {      //needs to implement another index for storing 
                body = {
                    to: _.map(params, item => item),
                    from: 'admin@factweavers.com',
                    subject: `Veraction - ${subjectMapper(alert.type).content}`,
                    text: 'Alert mail',
                    html: template.alert(subjectMapper(alert.type), alert)
                };
            }
            sgMail.send(body)
                .then((res) => {
                    Winston.AlertsEmailLogging(`Mail Send Successfully`, `email/alerts.js`, `30`);
                    callback(null, params);
                })
                .catch((err) => {
                    console.log('MAIL ERR', JSON.stringify(err, null, 4));
                    Winston.AlertsEmailLogging(`Mail Sending Failed`, 'email/alerts.js', '92')
                    callback(err);
                });
        } else {
            callback(null, params);
        }
    }
};