const async = require('async');
const helper = require('./helper');
const Winston = require('../logs/logger/winston');
const util = require('../utils');
const emails = require('config').emails;
const observingIps = require('config').observingIps;
module.exports = () => {
  async.waterfall([
    function checkObservingNodes(waterfallCallback) {
      util.pingObservingNodes(observingIps, (err, res) => {
        if (err) {
          waterfallCallback(err);
        } else {
          if ((res || []).length) {
            waterfallCallback(null, res);
          }
          waterfallCallback('error');
        }
      });
    },
    function sendMails(downIps, waterfallCallback) {
      let params = [{
        name: downIps,
        groups: emails,
        type: 'node_down_alert'
      }]
      //initiate email 
      helper.sendMailToGroups(params, (err, res) => {
        if (err) {
          waterfallCallback(err);
        } else {
          waterfallCallback(null, res);
        }
      });
    },
  ], (err) => {
    if (err) {
      if (err === 'error')
        Winston.InfoLogging('Observing Nodes is up',
          `alert/node_alert.js`, '39')
    } else {
      Winston.ErrorLogging('Observing Nodes is down',
        `alert/node_alert.js`, '42')
    }
  });
};