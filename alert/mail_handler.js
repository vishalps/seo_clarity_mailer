const mailer = require('../email/alerts');
const Winston = require('../logs/logger/winston');

module.exports = (alert) => {
  let groups = (alert && alert.groups) || [];
  groups.forEach((item, key) => {
    Winston.AlertsEmailLogging(`Alert Configured For Group: 
    ${item.group_name}; Subject: ${alert.subject}`,
      `alert/mailHandler.js`, `9`);
  })
  if (alert.type === 'node_down_alert') {
    mailer.sendMail(alert, alert.groups, (err) => {
      if (err) {
        Winston.ErrorLogging('Email Sending Failed',
          'alerts/mailHandler.js', '14');
      } else {
        Winston.GeneralLogging('Mail Handler - Mail Send \
        Successfully', `alerts/mailHandler.js`, '59');
      }
    });
  }
};