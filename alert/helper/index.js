const async = require('async');
const mailHandler = require('../mail_handler');
const Winston = require('../../logs/logger/winston');
module.exports = {
  sendMailToGroups: (params, callback) => {
    async.forEachOf(params, (item, key, eachCallback) => {
      switch (item.type) {
        case 'node_down_alert':
          if (item.groups.length > 0) {
            mailHandler(item);
          }
          eachCallback();
          break;

        default:
          eachCallback();
          break;
      }
    }, (err, res) => {
      if (err) {
        callback(err);
      } else {
        callback(null, res);
      }
    });
  }
}