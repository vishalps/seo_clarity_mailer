/* To log to console all ElasticQueries in the Project */
const { createLogger, format, transports } = require('winston');
require('winston-daily-rotate-file');
const { combine, printf, simple } = format;

/* Logger Transports */
const InfoFormat = printf(log_json => {
    return `[${log_json.date}]  [${log_json.level}] [${log_json.line}]  [${log_json.file}]  ${log_json.message}`
});
const InfoLogger = /* winston. */createLogger({
    format: combine(InfoFormat),
    transports: [
        new /* winston. */transports.DailyRotateFile({ filename: './logs/InfoLogs.log', maxFiles: '7d', maxSize: '15m', level: 'info' }),
        new /* winston. */transports.Console()
    ]
})

const ErrorFormat = printf(log_json => {
    return `[${log_json.date}]  [${log_json.level}]    [${log_json.line}]   [${log_json.file}]  ${log_json.message}`
});
const ErrorLogger = /* winston. */createLogger({
    format: combine(ErrorFormat),
    transports: [
        new /* winston. */transports.DailyRotateFile({ filename: './logs/ErrorLogs.log', maxFiles: '7d', maxSize: '15m', level: 'info' }),
        new /* winston. */transports.Console()
    ]
});

const DebugFormat = printf(log_json => {
    return `[${log_json.date}]  [${log_json.level}] [${log_json.line}]  [${log_json.file}]  ${log_json.message} ${log_json.text}`
});
const IndexFormat = printf(log_json => {
    return `[${log_json.date}]  [${log_json.level}] [${log_json.line}] [${log_json.message}] ${log_json.file} `
});
const DebugLogger = /* winston. */createLogger({
    format: combine(DebugFormat),
    transports: [
        new /* winston. */transports.DailyRotateFile({ filename: './logs/debug.log', maxFiles: '7d', maxSize: '15m', level: 'debug' }),
        new /* winston. */transports.Console()
    ]
})
const IndexLogger = /* winston. */createLogger({
    format: combine(IndexFormat),
    transports: [
        new /* winston. */transports.DailyRotateFile({ filename: './logs/vision.log', maxFiles: '7d', maxSize: '15m', level: 'info' }),
        new /* winston. */transports.Console()
    ]
})

const GeneralLogger = /* winston. */createLogger({
    format: simple(),
    transports: [
        new /* winston. */transports.Console()
    ]
})

const AlertEmailFormat = printf(log_json => {
    return `[${log_json.date}]  [${log_json.level}]    ${log_json.message}`
})
const AlertsEmailLogger = /* winston. */createLogger({
    format: combine(AlertEmailFormat),
    transports: [
        new /* winston. */transports.DailyRotateFile({ filename: './logs/Alerts-Email.log', maxSize: '15m', maxFiles: '7d' })
    ]
})

/* Logger Functions */
const InfoLogging = (message, file = null, line = null, date = new Date().toISOString()) => {
    InfoLogger.info({ date, file, line, message });
}
const DebugLogging = (message, file = null, line = null, date = new Date().toISOString()) => {
    DebugLogger.debug({ date, file, line, message })
}
const IndexLogging = (message, file = null, line = null,  date = new Date().toISOString()) => {
    IndexLogger.info({ date, file, line, message })
}
const ErrorLogging = (message, file = null, line = null, date = new Date().toISOString()) => {
    ErrorLogger.error({ date, file, line, message });
}
const GeneralLogging = (message, date = new Date().toISOString()) => {
    GeneralLogger.info(`[${date}] | ${message}`);
}
// const AlertsEmailLogging = (message, date = new Date().toISOString()) => {
//     AlertsEmailLogger.info(`[${date}] | ${message}`);
//     // AlertsEmailLogger[type]({ date, message });
// }
const AlertsEmailLogging = (message, file = null, line = null, date = new Date().toISOString()) => {
    AlertsEmailLogger.info({ date, file, line, message });
}
const ApiLogging = (message, file = null, line = null, date = new Date().toISOString()) => {
    APILogger.info({ date, file, line, message });
}

module.exports = {

    InfoLogging,
    DebugLogging,
    IndexLogging,
    ErrorLogging,

    GeneralLogging,
    AlertsEmailLogging

}