const config = require('config');
const cron = require('node-cron');
const node_down_alert = require('../alert/node_alert');

const expression = config.get('observingNodes.cron');
module.exports = cron.schedule(expression, () => {
  node_down_alert();
}, false);
